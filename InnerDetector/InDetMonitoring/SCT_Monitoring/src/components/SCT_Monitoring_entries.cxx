#include "../SCTTracksMonTool.h"
#include "../SCTLorentzMonTool.h"
#include "../SCTErrMonTool.h"
#include "../SCTHitEffMonTool.h"
#include "../SCTHitsNoiseMonTool.h"
#include "../SCTHitEffMonAlg.h"
#include "../SCTLorentzMonAlg.h"
#include "../SCTTracksMonAlg.h"

using namespace SCT_Monitoring;

DECLARE_COMPONENT( SCTTracksMonTool )
DECLARE_COMPONENT( SCTLorentzMonTool )
DECLARE_COMPONENT( SCTErrMonTool )
DECLARE_COMPONENT( SCTHitEffMonTool )
DECLARE_COMPONENT( SCTHitsNoiseMonTool )
DECLARE_COMPONENT( SCTHitEffMonAlg )
DECLARE_COMPONENT( SCTLorentzMonAlg )
DECLARE_COMPONENT( SCTTracksMonAlg )

